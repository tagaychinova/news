# Frontend Software Engineer – Homework

## Demo

http://eat-sleep-code.ru/

## Getting Started

To get you started you can simply clone the repository using git

### Install Dependencies

```
yarn install
```

## Directory Layout

```
src/                    --> all of the source files for the application
  components/           --> all app specific modules
  services/                --> contains mock services (for getting, adding and removing news)
  main.ts                --> main application module
  index.html            --> app layout file (the main html template file of the app)
```

### Running the App during Development

The project comes preconfigured with a local development webserver. It is a webpack-dev-server, that supports hot reload.  You can start this webserver with `npm start`.

Now browse to the app at `http://localhost:8080/`.

### Building and running the App in Production

To build the application for production just run `npm build`, it creates dist directory that have the production optimized build.

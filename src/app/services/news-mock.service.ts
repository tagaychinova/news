import { IPromise } from 'angular';
import { News } from '../types';
import { INewsService } from './news.service.interface';

export class NewsMockService implements INewsService {
  private lastId = 3;

  private news: News[] = require('./mockData.json');

  public getNews(): IPromise<News[]> {
    return Promise.resolve([...this.news]);
  }

  public addNews(title: string, content: string): Promise<boolean> {
    const news: News = {
      id: ++this.lastId,
      title,
      content,
      status: 'New',
    };

    this.news.unshift(news);

    return Promise.resolve(true);
  }

  removeNews(ids: number[]): IPromise<boolean> {
    this.news = this.news
      .filter(n => ids.indexOf(n.id) === -1);

    return Promise.resolve(true);
  }
}

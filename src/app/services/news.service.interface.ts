import { IPromise } from 'angular';
import { News } from '../types';

export interface INewsService {
  getNews(): IPromise<News[]>;
  addNews(title: string, content: string): IPromise<boolean>;
  removeNews(ids: number[]): IPromise<boolean>;
}

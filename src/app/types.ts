export type Status = 'New' | 'Completed' | 'Not completed';

export interface News {
  id: number;
  title: string;
  content: string;
  status: Status;
}

import { INewsService } from '../services';
import { News } from '../types';

export interface IMainScope extends ng.IScope {
  title: string;
  news: News[];
  showAddNewsDialog: boolean;
}

export class MainCtrl {
  static $inject = ['$scope', 'NewsService'];

  constructor(protected $scope: IMainScope, private newsService: INewsService) {
    this.$scope = $scope;
    $scope.news = [];
    $scope.showAddNewsDialog = false;

    this.loadNews();
  }

  public addNewsHandler = async (title: string, content: string) => {
    this.$scope.showAddNewsDialog = false;

    const isSuccess = await this.newsService
      .addNews(title, content);

    if (isSuccess) {
      this.loadNews();
    }
  }

  public openAddNewsDialog = () => {
    this.$scope.showAddNewsDialog = true;
  }

  public closeAddNewsDialog = () => {
    this.$scope.showAddNewsDialog = false;
  }

  public removeNewsHandler = async (ids: number[]) => {
    const isSuccess = await this.newsService.removeNews(ids);

    if (isSuccess) {
      this.loadNews();
    }
  }

  private loadNews = () => {
    this.newsService.getNews()
      .then(
        (data) => {
          this.$scope.news = data;
          this.$scope.$apply();
        },
      );
  }
}

import { IComponentOptions } from 'angular';

import { RemoveNewsController } from './remove-news.controller';

export class RemoveNewsComponent implements IComponentOptions {
  public controller = RemoveNewsController;
  public template = require('./remove-news.html');
  public bindings = { okHandler: '<', closeHandler: '<' };
  public styles = [require('./remove-news.scss')];
}

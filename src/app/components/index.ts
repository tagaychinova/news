export { NewsSummaryComponent } from './news-summary/news-summary.component';
export { NewsListComponent } from './news-list/news-list.component';
export { AddNewsComponent } from './add-news/add-news.component';
export { RemoveNewsComponent } from './remove-news/remove-news.component';

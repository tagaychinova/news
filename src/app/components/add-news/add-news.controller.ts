export class AddNewsController {
  public title: string;
  public content: string;
  public addNewsHandler: (title: string, content: string) => void;
  public closeHandler: () => void;

  public submit() {
    this.addNewsHandler(this.title, this.content);
  }
}

import { IComponentOptions } from 'angular';

import { AddNewsController } from './add-news.controller';

export class AddNewsComponent implements IComponentOptions {
  public controller = AddNewsController;
  public template = require('./add-news.html');
  public bindings = { addNewsHandler: '<', closeHandler: '<' };
  public styles = [require('./add-news.scss')];
}

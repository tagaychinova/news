import { News, Status } from '../../types';

export interface INewsSummaryScope extends ng.IScope {
  title: string;
}

interface ISummaryItem {
  header: Status|'Total';
  getValue: () => number;
}

export class NewsSummaryController {
  public summaryItems: ISummaryItem[] = [
    {
      header: 'Total',
      getValue: () => this.news ? this.news.length : 0,
    },
    this.getSummaryItemByStatus('Completed'),
    this.getSummaryItemByStatus('Not completed'),
  ];

  private news: News[];

  private getSummaryItemByStatus(status: Status): ISummaryItem {
    return {
      header: status,
      getValue: () => {
        if (!this.news) {
          return 0;
        }

        return this.news.filter(n => n.status === status).length;
      }
    };
  }
}

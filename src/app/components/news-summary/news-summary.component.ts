import { IComponentOptions } from 'angular';

import { NewsSummaryController } from './news-summary.controller';

export class NewsSummaryComponent implements IComponentOptions {
  public controller = NewsSummaryController;
  public template = require('./news-summary.html');
  public bindings = { news: '<' };
  public styles = [require('./news-summary.scss')];
}

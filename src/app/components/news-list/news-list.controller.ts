import { reverse, sortBy } from 'lodash';

import { News } from '../../types';

interface Row {
  checked: boolean;
  news: News;
}

interface Column {
  header: string;
  className: string;
  getValue: (news: News) => string | number;
}

enum SortDirection {
  Asc,
  Desc,
}

export class NewsListController implements ng.IController {
  static $inject = ['$scope'];

  public rows: Row[] = [];
  public news: News[];
  public checkedAll = false;

  public columns: Column[] = [
    {
      header: 'ID',
      className: 'id-column',
      getValue: news => news.id,
    },
    {
      header: 'Title',
      className: 'title-column',
      getValue: news => news.title,
    },
    {
      header: 'Content',
      className: 'content-column',
      getValue: news => news.content,
    },
    {
      header: 'Status',
      className: 'status-column',
      getValue: news => news.status,
    }
  ];

  public sortByColum: Column = null;
  public sortDirection: SortDirection = null;
  public showRemoveNewsDialog = false;
  public removeNewsHandler: (ids: number[]) => void;

  constructor(protected $scope: ng.IScope) {}

  $onChanges() {
    this.showRemoveNewsDialog = false;
    this.checkedAll = false;

    if (!this.news) {
      return;
    }

    this.rows = this.news.map(n => ({
      checked: false,
      news: n,
    }));

    if (this.sortByColum) {
      this.onSort(this.sortByColum);
    }
  }

  public onCheckAllClick() {
    if (!this.rows) {
      return;
    }

    this.checkedAll = !this.checkedAll;

    this.showRemoveNewsDialog = this.checkedAll;

    this.rows.forEach(row => {
      row.checked = this.checkedAll;
    });
  }

  public onCheckClick(row: Row) {
    const isAllChecked = !this.rows.find(r => !r.checked);
    const isAllUnchecked = !this.rows.find(r => r.checked);

    if (row.checked) {
      this.showRemoveNewsDialog = true;
    } else if (isAllUnchecked) {
      this.showRemoveNewsDialog = false;
    }

    if (isAllChecked) {
      this.checkedAll = true;
    }

    if (isAllUnchecked) {
      this.checkedAll = false;
    }
  }

  public isIndeterminateCheckAllState() {
    if (!this.rows) {
      return false;
    }

    const isSomeChecked = this.rows.find(r => r.checked);
    const isSomeUncheked = this.rows.find(r => !r.checked);

    return isSomeChecked && isSomeUncheked;
  }

  public onSort(column: Column) {
    if (!column) {
      return;
    }

    this.rows = sortBy(this.rows, r => column.getValue(r.news));

    if (this.sortByColum === column && this.sortDirection === SortDirection.Asc) {
      this.sortDirection = SortDirection.Desc;
      this.rows = reverse(this.rows);
      return;
    }

    this.sortByColum = column;
    this.sortDirection = SortDirection.Asc;
  }

  public getColumnClass(column: Column) {
    const classNames = [column.className];

    if (column === this.sortByColum) {
      classNames.push(this.sortDirection === SortDirection.Desc ? 'desc' : 'asc');
    }

    return classNames.join(' ');
  }

  public removeNewsCofirmed = () => {
    this.showRemoveNewsDialog = false;

    const ids = this.rows
      .filter(r => r.checked)
      .map(r => r.news.id);

    this.removeNewsHandler(ids);
  }

  public openRemoveNewsDialog = () => {
    this.showRemoveNewsDialog = true;
  }

  public closeRemoveNewsDialog = () => {
    this.showRemoveNewsDialog = false;
  }
}

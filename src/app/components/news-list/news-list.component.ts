import { IComponentOptions } from 'angular';

import { NewsListController } from './news-list.controller';

export class NewsListComponent implements IComponentOptions {
  public controller = NewsListController;
  public template = require('./news-list.html');
  public bindings = { news: '<', removeNewsHandler: '<' };
  public styles = [require('./news-list.scss')];
}

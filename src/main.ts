import * as angular from 'angular';
import { AddNewsComponent, NewsListComponent,
  NewsSummaryComponent, RemoveNewsComponent } from './app/components/';
import { MainCtrl } from './app/controllers/MainCtrl';
import { NewsMockService } from './app/services';

import './app/styles.scss';

angular.module('AppModule', [])
  .service('NewsService', NewsMockService)
  .controller('MainCtrl', MainCtrl)
  .component('newsSummary', new NewsSummaryComponent())
  .component('newsList', new NewsListComponent())
  .component('addNews', new AddNewsComponent())
  .component('removeNews', new RemoveNewsComponent());
